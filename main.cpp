#include <iostream>
#include <string>
#include <cstdlib>//rand()
#include <ctime>//srand()
#define N 5
using namespace std;

//A[i][j],i:row;j:column
int A[N][N];
int B[N][N];
int A1[N][N];
int A2[N][N];
int A3[N][N];
int A4[N][N];

void checkEquality();//use to check equalit
void tr(string a);//transpose Matrix
void ques1();//solve question 1
void ques2();//solve question 2

int main()
{
	srand(time(NULL));
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
		{
			A[i][j]=rand()%10000;
			B[i][j]=rand()%10000;
		}
	}

	/*ques1*/
	cout<<"Question 1"<<endl;
	ques1();
	checkEquality();
	
	/*ques2*/
	cout<<endl<<"Question2"<<endl;
	ques2();
	checkEquality();

	/*print A and B*/
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
			cout<<" "<<A1[i][j];
		cout<<endl;
	}
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
			cout<<" "<<A3[i][j];
		cout<<endl;
	}
	

}


void ques1()
{	
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
		{
			for(int k=0;k<N;++k)
			{
				A1[i][j]+=A[i][k]*B[k][j];
				A2[i][j]+=B[i][k]*A[k][j];
				A3[j][i]+=A[k][i]*B[j][k];
				A4[j][i]+=B[k][i]*A[j][k];
			}
		}
	}
}

void ques2()
{
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
		{
			for(int k=0;k<N;++k)
			{
				tr("B");
				A1[i][j]+=A[i][k]*B[j][k];
				tr("B");
				tr("A");
				A2[i][j]+=A[i][k]*B[k][j];
				A3[i][j]+=B[i][k]*A[k][j];
				tr("A");
				tr("B");
				A4[i][j]+=B[i][k]*A[k][j];
				tr("B");
			}
		}
	}
	tr("A3");
	tr("A4");
}

void checkEquality()
{
	bool A1nA2=0;
	bool A1nA3=0;
	bool A1nA4=0;
	bool A2nA3=0;
	bool A2nA4=0;
	bool A3nA4=0;
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
		{
			if(A1[i][j]!=A2[i][j])
				A1nA2=1;
			if(A1[i][j]!=A3[i][j])
				A1nA3=1;
			if(A1[i][j]!=A4[i][j])
				A1nA4=1;
			if(A2[i][j]!=A3[i][j])
				A2nA3=1;
			if(A2[i][j]!=A4[i][j])
				A2nA4=1;
			if(A3[i][j]!=A4[i][j])
				A3nA4=1;
		}
	}
	cout<<"A1 is ";
	if(A1nA2==0)
		cout<<"equal to A2."<<endl;
	else
		cout<<"not equal to A2."<<endl;
	cout<<"A1 is ";
	if(A1nA3==0)
		cout<<"equal to A3."<<endl;
	else
		cout<<"not equal to A3."<<endl;
	cout<<"A1 is ";
	if(A1nA4==0)
		cout<<"equal to A4."<<endl;
	else
		cout<<"not equal to A4."<<endl;
	cout<<"A2 is ";
	if(A2nA3==0)
		cout<<"equal to A3."<<endl;
	else
		cout<<"not equal to A3."<<endl;
	cout<<"A2 is ";
	if(A2nA4==0)
		cout<<"equal to A4."<<endl;
	else
		cout<<"not equal to A4."<<endl;
	cout<<"A3 is ";
	if(A3nA4==0)
		cout<<"equal to A4."<<endl;
	else
		cout<<"not equal to A4."<<endl;
}

void tr(string a)
{
	int copy[N][N];
	if(a== "A")
	{
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				copy[i][j]=A[i][j];
		}
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				A[i][j]=copy[j][i];
		}
	}
	else if(a=="B")
	{
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				copy[i][j]=B[i][j];
		}
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				B[i][j]=copy[j][i];
		}
	}
	else if(a=="A1")
	{
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				copy[i][j]=A1[i][j];
		}
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				A1[i][j]=copy[j][i];
		}
	}
	else if(a=="A2")
	{
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				copy[i][j]=A2[i][j];
		}
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				A2[i][j]=copy[j][i];
		}
	}
	else if(a=="A3")
	{
	  for(int i=0;i<N;++i)
	  {
		  for(int j=0;j<N;++j)
			  copy[i][j]=A3[i][j];
	  }
	  for(int i=0;i<N;++i)
	  {
		  for(int j=0;j<N;++j)
			  A3[i][j]=copy[j][i];
	  }
	}
	else if(a=="A4")
	{
		for(int i=0;i<N;++i)
		{
			for(int j=0;j<N;++j)
				copy[i][j]=A4[i][j];
		}
		for(int i=0;i<N;++i)		   
		{
			for(int j=0;j<N;++j)
				A1[i][j]=copy[j][i];
		}
	}
}
